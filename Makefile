#
# Image
#

image-plain:
	docker build -t padm-project-2022f:latest $(shell pwd)/padm-project-2022f

image-nvidia:
	$(warning Building assuming using NVIDIA gpus! Change make target if otherwise!)
	docker build -t padm-project-2022f:latest -f Nvidiafile .

# Change to image-plain if not using NVIDIA gpus
image: image-nvidia

clean-image:
	docker rmi padm-project-2022f:latest

#
# Dev
#

jupyter:
	docker run -it --rm -e DISPLAY=$(DISPLAY) --gpus all -v /tmp/.X11-unix/:/tmp/.X11-unix --network=host -v $(shell pwd):/appsrc -w /appsrc padm-project-2022f:latest /bin/bash -c "pip install jupyter && jupyter notebook --allow-root"

dev:
	docker run -it --rm --network=host -v $(shell pwd):/appsrc -w /appsrc padm-project-2022f:latest bash

#
# Project
#

plan:
	docker run -it --rm --gpus all -v $(shell pwd):/appsrc -w /appsrc padm-project-2022f:latest python -B -m ehc.solve domain.pddl base_prob.pddl

traj:
	docker run -it --rm --gpus all -v $(shell pwd):/appsrc -w /appsrc padm-project-2022f:latest python -B -m trajectory_optimization.traj_test

rrt:
	docker run -it --rm --gpus all -v $(shell pwd):/appsrc -w /appsrc padm-project-2022f:latest python -B -m motion_planner.rrt_test

#
# Demonstration
#

pddl-examples:
	docker run -it --rm -e DISPLAY=$(DISPLAY) --gpus all -v /tmp/.X11-unix/:/tmp/.X11-unix --network=host -v $(shell pwd):/appsrc -w /appsrc padm-project-2022f:latest /bin/bash pddl-examples.sh

viz-example:
	xhost +local:root
	docker run -it --rm -e DISPLAY=$(DISPLAY) --gpus all -v /tmp/.X11-unix/:/tmp/.X11-unix --network=host -v $(shell pwd):/appsrc -w /appsrc padm-project-2022f:latest /bin/bash -c "cd padm-project-2022f && python3 minimal_example.py"
#
# Defaults
#

example: pddl-examples viz-example

project: plan traj rrt

clean: clean-image

#
# All target glob
#

demo: image example

all: image project
