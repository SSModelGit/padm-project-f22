# padm-project-f22

## Description
This is the repository for our approach to the final project of the PADM F'22 class. Authors and acknowledgments listed below. Follow the Installation section below for more details on getting started. A more formalized (i.e. Latex-ized) version of the README is available at [here](write_ups/16_413_Final_Writeup.pdf).

**Note**: A more finalized write-up was produced after the deadline. The crucial difference between the write-ups is that videos of the full plan being completed by the robot (under both sampling-based methods and trajectory optimized methods) were uploaded and added to the later write-up. Please read this write-up as well, available [here](write_ups/16_413_Final_Writeup_With_Full_Videos.pdf).

## Installation
For a system running `bash`, `docker` with GPU integration completed, and `make`, the following scripts are useful for initial setup after the `git clone`:

* `setup.sh` creates the initial files and metadata needed for the project.
* `setup-docker.sh` wraps the above setup script, and includes running the initial `docker` build of the system. This is for those who are using `docker` to run the project, as opposed to native project hosting.

## Design Choices, Operation, and General Usage

* Activity Planner
    * We used separate types for drawer/surface to allow us to distinguish between what objects can and cannot be opened
        * This allowed us to use type checking to distinguish between valid actions instead of using a more general object and adding attribute predicates
        * However, it does mean that there are duplicate predicates for the different types of objects that answer the same question (such as "at")
    * We assumed that if the robot is next to an object (such as a surface, drawer, or box) it can interact with it
        * This assumption may prove not to hold in all cases depending on how much room is required to open a drawer

The activity planner currently implemented is Enforced Hill Climbing (with the Fast-Forward Heuristic). The files pertaining to the planner are bundled inside the `ehc` folder. They are structured as a Python library, allowing quick import access.

The file tree for `ehc` is as follows:
```bash
ehc
├── __init__.py
├── ehc_planner.py
├── solve.py
└── utils.py
```

The `utils.py` file is meant as an internal file, not used by the end-user. This contains core data structures, such as the "environment model" (named `World`, appropriately), and the linked list representation used during back-tracking (`PredNode` and `Node`).

The `ehc_planner.py` is the higher-level activity planner that actually implements the EHC method; however, it is still intended as a middle-layer tool, and is structured with "developers" in mind, as opposed to the end-user. In it are two critical algorithms - the base Hill-Climbing algorithm, and the Fast-Forward heuristic.

Both use Linked-Lists to indicate the relation between states (and the actions linking them) in the exploration tree, be it the relaxed graph for the heuristic approach or the fully-restricted graph for the main tree. As the relaxed graph has a different "growth" method than the fully-restricted graph, the nodes used to represent the lists are different, leading to the two node types discussed earler. `PredNode` is for the heuristic solver, and `Node` is for the full solver. `Node`s are representations of states, and consist of the state's positive predicates, the parent state, the action that moved the parent state to the current state, and the cost accrued from start to the current state. The link to parent nodes creates the linked list structure; at the root of the list is the initial state, initialized with no parents and no cost. `PredNode`s are representations of either the positive predicates in any fact layer during the FF solving process, or the action connecting two predicates in different fact layers. They are represented by the node information, either predicate or action, a boolean indicating if the information is an action or not, and a list of all parents that needed to be satisfied to give rise to the current node (for an action, these are its positive preconditions; for a predicate, it is the action the predicate is a positive effect of). The link to the parents are once again the cause of the linked list, and the tree root is described similar to that of the `Node`.

Within `ehc_planner.py`, two classes are provided, with one being dedicated as a "helper" class. This helper class is `FF`, and defines the process of solving the relaxed problem graph; it is used as a helper to the method `._solve_FF()` in the main planner class `EHC`. Along with `._solve_FF()` two other solving methods are provided - one which arrives at the true optimal solution via uninformed BFS, `._solve_BFS_opt()`, and one which brute-forces a semi-greedy approach to BFS, `._solve_BFS()`. The latter of these two methods is also found similarly in `PDDL_Parser`'s library, and hence despite its reliability it is not used as the primary planner; the main difference between that method and our implementation is that `._solve_BFS()` incorporates our `Node` linked list and intelligent hashing to accomplish a minor reduction in overall node space search times, at the cost of increased post-processing time and spatial complexity. The `._solve_BFS_opt()` method is not a feasible approach many of the times, evidenced by the NP-hard problem space complexity; in practice, it tends to blow up in size exponentially and rapidly exceed kernel/RAM limitations, causing kernel and memory faults. However, given enough time and computational resources, and assuming that a solution does exist, the true optimal solution will be found. This can be used to guarantee the solvability of a problem in face of other potentially faulty algorithms; it can also be used to compare the optimality of any of our other approximate algorithms to the true optimal solution.

The `._solve_BFS()` approach uses best-first-search, and stores cost information and such within the `Node` objects; once the goal is first reached, it backtracks from the current goal node till the root, and returns the states and actions along the path. The `._solve_FF()` approach is also best-first-search, but the defining cost-function is the combination of both the node's accumulated cost and the heuristic cost between the current node and the end goal. This invokes the `FF` class's algorithm. Once the goal has been reached within the `._solve_FF()` algorithm, the backtracking and results returning is identical to that of `._solve_BFS()`. Within the `FF` class, the search for solutions is slightly more complicated. The heuristic algorithm continues enacting permissible actions until no actions are further possible; with no delete effects in place, this inevitably leads to either a superset of predicates that containts the goal partial state, or to a scenario where no more actions are possible without having reached the goal state, indicating that no solution is possible at all. In the latter case, an extraordinarily value is returned, to simulate infinity and indicate that the state is not feasible. In the former case, from the goal predicates the nodes are backtracked, going back through their corresponding actions and listing all the actions required to eventually achieve the goal state. A list is maintained to ensure that actions aren't double counted while following the multiple paths from goal predicates to root. The sum total of actions is returned as the heuristic value.

### API
The `Planner` class within `solve.py` is the intended end-user class that wraps the entire activity planning operation, and enables user-friendly results displaying.

 * It is initialized with the signature `Planner(domain, problem, plan_type="ehc")`. The `domain` and `problem` fields are the filename paths to the `.pddl` files specifying the activity domain and problem; the `plan_type` defaults to `"ehc"`, and should be left as such - it is included now to prepare for the future possibility of different, non-EHC planners.

 * The problem can be solved after instatiation using `Planner.solve(style="approx", disp_results=True)`. The field `style` defaults to `"approx"`, representing the core approximation of any planner; for `"ehc"`, it is the EHC-FF method `EHC._solve_FF()`. Two other strings can be passed to `style` - `"opt"`, representing the true optimal planner incorporated in the planner, and `"brute"`, representing the brute-force approach incorporated in the planner. For `"ehc"`, these are `EHC._solve_BFS_opt()` and `EHC._solve_BFS()`, respectively. The field `disp_results` is a boolean indicating if the plan results should be formatted and printed after returning, or not. The function's return value is an exit code indicating if the planning succeeded or not.

 * Separate from the planning, there is a convenience method with signature `Planner.show(verbose=False)`. This "pretty prints" the solved plan, but will assert an error if invoked before running `Planner.solve` at lease once. The `verbose` is an optional field - when `True`, it will display all action information for each action in the solution plan.

 * After solving, the class instance will also have two observable attributes set - `Planner.plan`, either containing the solved plan or `None`, and `self.a_list` and `self.s_list`, the list of actions and states parsed from the returned plan, respectively. Note that the latter two only occur when the plan has been solved successfully.

Along with the `Planner` class, `solve.py` can also be used from the command line, with the command:

```
cd padm-project-f22
python -B -m ehc.solve /path/to/domain.pddl /path/to/problem.pddl
```

This will solve the problem specified by (`domain.pddl`,`problem.pddl`) using the class `Planner` described above, and will pretty print the domain specification, problem description, and the solution plan. There are two optional flags that can also be passed after the domain and problem filenames:
 * `-opt`: This invokes the true optimal solver on the problem. Beware that this will take at the very least a very long time, and at worst can result in program crash.
 * `-bfs`: This invokes the brute force algorithm, and can be used to verify that a problem is solvable at all. Note, however, that this only can truly indicate that a problem has a solution - it cannot be used to definitely say that there is no solution, given the greedy nature of the approach.

### Using Make
For those using `docker` to run the project, the above functionality can be quickly accessed using the provided `Makefile`. Below are relevant targets:

- Low-level user commands:
  - `image`: This builds the docker image. By default, this is set to build an image that relies on Nvidia GPUs for visualization.
    - Edit the`image` target in the `Makefile` to use `image-plain` instead of `image-nvidia` if you prefer to not use Nvidia GPUs.
  - `dev`: This opens the built project image as a shell instance. The container will be removed after stopping, so ensure persistent changes occur in the mounted folder `/appsrc`.
  - `jupyter`: This opens a shell instance like in the `dev` target, and then starts a Jupyter kernel instance at the `/appsrc` directory in the image. The container will be removed after stopping.
- High-level user commands:
  - `example`: This runs our planner on all the PDDL example problem specifications, and checks to make sure the planner is not breaking. It then runs the base visualization example to ensure the project container instance works properly.
  - `project`: This runs all the targets for our project pipeline (listed below in order).. For now, the only implemented portion is the activity planner.
    - `plan`: This solves the activity planing problem for our project specification.
- Client-level commands:
  - `demo`: This builds the project image and then runs the `example` target. Use to test the full pipeline.
  - `all`: This builds the project image and then runs the `project` target. Use to run a complete, fresh pipeline instance.
  - `clean`: This removes any build files from the host machine. At the moment, this only removes the built `docker` image.

## Authors and acknowledgment
Shashank Swaminathan, Jake Olkin

## [TODO] License
MIT License (aspirationally, not actually added).

## Project status
In development.
