(define (problem base)
    (:domain padm)
    (:objects
        r1 - robot
        l1 l2 l3 l4 - location
        sugar spam - box
        burner counter - surface
        indigo - drawer
    )
    (:init
        (adjacent l1 l2) (adjacent l1 l3) (adjacent l2 l4) (adjacent l3 l4) (adjacent l2 l1)
        (at r1 l1)
        (atSurface burner l3) (atSurface counter l4)
        ;(emptySurface counter)
        (atDrawer indigo l4)
        (on sugar burner) (on spam counter)
        (emptyDrawer indigo)
        (emptyhanded r1)
        (occupied l3) (occupied l4)
    )

    (:goal (and (on sugar counter) (in spam indigo)))
    ;(:goal (on sugar counter))
)
