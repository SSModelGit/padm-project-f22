(define
    (domain padm)
    (:requirements :strips :typing :negative-preconditions)

    (:types
        location ; locations in the world.
        robot ; the robot
        box ; a box (only carriable object)
        surface ; a place to put a box that is not openable
        drawer ; a place to put a box that is openable
    )

    (:predicates
        (adjacent ?l1 ?l2 - location)
        (atRobot ?r - robot ?l - location)
        (atSurface ?s - surface ?l - location)
        (atDrawer ?d - drawer ?l - location)
        (holding ?r - robot ?b - box)
        (on ?b - box ?s - surface)
        (in ?b - box ?d - drawer)
        (occupied ?l - location)
        (emptyhanded ?r - robot)
        (open ?d - drawer)
        (emptySurface ?s - surface)
        (emptyDrawer ?d - drawer)
    )

    (:action move
        :parameters (?r - robot ?from - location ?to - location)
        :precondition (and (adjacent ?from ?to) (at ?r ?from) (not (occupied ?to)))
        :effect (and (at ?r ?to) (not (at ?r ?from)))
    )

    (:action pick
        :parameters (?r - robot ?l1 - location ?l2 - location ?b - box ?s - surface)
        :precondition (and (adjacent ?l1 ?l2) (at ?r ?l1) (atSurface ?s ?l2) (on ?b ?s) (emptyhanded ?r))
        :effect (and (holding ?r ?b) (not (emptyhanded ?r)) (not (on ?b ?s)) (emptySurface ?s))
    )

    (:action put
        :parameters (?r - robot ?l1 - location ?l2 - location ?b - box ?s - surface)
        :precondition (and (adjacent ?l1 ?l2) (at ?r ?l1) (atSurface ?s ?l2) (emptySurface ?s) (holding ?r ?b))
        :effect (and (not (holding ?r ?b)) (not (emptySurface ?s)) (emptyhanded ?r) (on ?b ?s))
    )

    (:action open
        :parameters (?r - robot ?l1 - location ?l2 - location ?d - drawer)
        :precondition (and (adjacent ?l1 ?l2) (at ?r ?l1) (atDrawer ?d ?l2) (not (open ?d)) (emptyhanded ?r))
        :effect (open ?d)
    )

    (:action close
        :parameters (?r - robot ?l1 - location ?l2 - location ?d - drawer)
        :precondition (and (adjacent ?l1 ?l2) (at ?r ?l1) (atDrawer ?d ?l2)  (open ?d))
        :effect (not (open ?d))
    )

    (:action take
        :parameters (?r - robot ?l1 - location ?l2 - location ?b - box ?d - drawer)
        :precondition (and (adjacent ?l1 ?l2) (at ?r ?l1) (atDrawer ?d ?l2) (in ?b ?d) (emptyhanded ?r) (open ?d))
        :effect (and (holding ?r ?b) (not (emptyhanded ?r)) (not (in ?b ?d)) (emptyDrawer ?s))
    )

    (:action stow
        :parameters (?r - robot ?l1 - location ?l2 - location ?b - box ?d - drawer)
        :precondition (and (adjacent ?l1 ?l2) (at ?r ?l1) (atDrawer ?d ?l2) (holding ?r ?b) (open ?d) (emptyDrawer ?d))
        :effect (and (not (holding ?r ?b))  (emptyhanded ?r) (in ?b ?d) (not (emptyDrawer ?s)))
    )
)

