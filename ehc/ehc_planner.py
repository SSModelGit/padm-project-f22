from heapq import heappop, heappush

from .utils import *

class FF:
    def __init__(self, world):
        self.w = world
        self.goal = self.w.goal

    def full_state(self, predicates):
        return frozenset([getattr(pn,'info') for pn in predicates])

    def init_predicates(self, state):
        return dict([[p, PredNode(p,parents=None,isaction=False)] for p in state])

    def applicate(self, preds, action):
        plist = []
        for cond in action.positive_preconditions:
            if preds.get(cond) == None:
                return False, None, None
            plist.append(preds.get(cond))
        anode = PredNode(action,parents=plist,isaction=True)
        pdlist = []
        exists = True # Ignore the action if its effects are already done
        for add in action.add_effects:
            if preds.get(add) == None:
                exists = False
                pdlist.append([add, PredNode(add, parents=[anode])])
        if exists:
            return False, None, None
        return True, anode, dict(pdlist)

    def isgoal(self, preds):
        for g in self.goal:
            # print("%s exists: %s" % (str(g), str(preds.get(g))))
            if preds.get(g) == None:
                return False
        return True

    def backtrack(self, exp, preds, acts, curr):
        if curr.parents == None:
            return 0
        score = 0
        if curr.sq == True:
            for par in curr.parents:
                if par not in exp:
                    exp.append(par)
                    score += self.backtrack(exp, preds, acts, par)
            return score + 1
        else:
            if curr.parents[0] not in exp:
                exp.append(curr.parents[0])
                return self.backtrack(exp, preds, acts, curr.parents[0])
            else:
                return 0

    def retrieve_score(self, preds, acts):
        expanded = list()
        score = 0
        for g in self.goal:
            expanded.append(preds.get(g))
            score += self.backtrack(expanded, preds, acts, preds.get(g))
        # print("Score is: {}".format(str(score-1)))
        return score-1 # Take off one because initial actions shouldn't be counted

    def ff(self, start_node, start_action):
        # initialize
        preds = dict([[p,PredNode(p,parents=None,isaction=False)] for p in start_node.state])
        exists, first_act, new_preds = self.applicate(preds, start_action)
        if not exists:
            return infty
        acts = list([first_act])
        preds = {**preds, **new_preds}
        while 1:
            new_preds = dict()
            exhausted = True
            for act in self.w.g_acts:
                pna, pnb, pnc = self.applicate(preds, act)
                if pna:
                    exhausted = False
                    new_preds = {**new_preds, **pnc}
                    acts.append(pnb)
                    if self.isgoal({**preds,**new_preds}):
                        return self.retrieve_score({**preds,**new_preds}, acts)
            if exhausted:
                return infty
            preds = {**preds, **new_preds}

class EHC:
    def __init__(self, domain, problem):
        self.w = World(domain, problem)
        self.w.ground_actions()
        self.start = self.w.start
        self.goal = self.w.goal
        self.ff = FF(self.w)

    def retrieve_plan(self, state):
        if state.parent == None:
            return list()
        return self.retrieve_plan(state.parent) + [[state.state,state.action]]

    def isgoal(self, state):
        return self.goal.issubset(state.state)

    def applicable(self, state, action):
        return action.positive_preconditions.issubset(state) and action.negative_preconditions.isdisjoint(state)

    def apply(self, state, action):
        return state.difference(action.del_effects).union(action.add_effects)

    def expand_node(self, node):
        expanded = list()
        for act in self.w.g_acts:
            if self.applicable(node.state, act):
                expanded.append(Node(self.apply(node.state,act),
                                     action=act,
                                     cost=1+node.cost,
                                     parent=node))
        return expanded

    def _solve_BFS_opt(self):
        self.nsq = []
        i = 0
        heappush(self.nsq,(self.start.cost, i, self.start))
        i+=1
        self.expanded = set([self.start])
        while self.nsq:
            _, _, node = heappop(self.nsq)
            self.expanded.add(node)
            if self.isgoal(node):
                return self.retrieve_plan(node)
            for nx_node in self.expand_node(node):
                if nx_node not in self.expanded:
                    heappush(self.nsq,(nx_node.cost,
                                       i,
                                       nx_node))
                    i+=1
        return None

    def _solve_BFS(self):
        self.nsq = []
        i = 0
        heappush(self.nsq,(self.start.cost, i, self.start))
        i+=1
        self.expanded = set([self.start])
        while self.nsq:
            _, _, node = heappop(self.nsq)
            if self.isgoal(node):
                return self.retrieve_plan(node)
            for nx_node in self.expand_node(node):
                if nx_node not in self.expanded:
                    heappush(self.nsq,(nx_node.cost,
                                       i,
                                       nx_node))
                    i+=1
                    self.expanded.add(nx_node)
        return None

    def _solve_FF(self):
        self.nsq = []
        i = 0
        heappush(self.nsq,(self.start.cost, i, self.start))
        i+=1
        self.expanded = set([self.start])
        while self.nsq:
            # print(len(self.nsq))
            _, _, node = heappop(self.nsq)
            self.expanded.add(node)
            if self.isgoal(node):
                return self.retrieve_plan(node)
            for nx_node in self.expand_node(node):
                if nx_node not in self.expanded:
                    heappush(self.nsq,(nx_node.cost+self.ff.ff(node, nx_node.action),
                                       i,
                                       nx_node))
                    i+=1
        return None

    def solve(self, brutus=False, opt=False):
        if opt:
            return self._solve_BFS_opt()
        if brutus:
            return self._solve_BFS()
        return self._solve_FF()
