from .ehc_planner import *
from pddl_parser.PDDL import PDDL_Parser

class Planner:
    def __init__(self, domain, problem, plan_type="ehc"):
        if plan_type=="ehc":
            self.solver = EHC(domain, problem)
        self.plan = None
        self.failure = False

    def solve(self, style="approx", disp_results=True):
        if style=="opt":
            self.plan = self.solver.solve(opt=True)
        elif style=="brute":
            self.plan = self.solver.solve(brutus=True)
        else:
            self.plan = self.solver.solve()

        self.failure = False if type(self.plan) == list else True
        if not self.failure:
            self.a_list = [pla[1] for pla in self.plan]
            self.s_list = [pla[0] for pla in self.plan]

        if disp_results==True:
            self.show()

        return self.failure

    def show(self, verbose=False):
        assert self.plan != None or self.failure != False, "Plan not found yet"
        if not self.failure:
            print("plan:")
            for act in self.a_list:
                print(act if verbose else act.name + ' ' + ' '.join(act.parameters))
        else:
            print("no plan was found")

if __name__ == '__main__':
    import sys, pprint, time
    domain = sys.argv[1]
    problem = sys.argv[2]
    parser = PDDL_Parser()
    print('----------------------------')
    pprint.pprint(parser.scan_tokens(domain))
    print('----------------------------')
    pprint.pprint(parser.scan_tokens(problem))
    print('----------------------------')
    parser.parse_domain(domain)
    parser.parse_problem(problem)
    print('Domain name: ' + parser.domain_name)
    for act in parser.actions:
        print(act)
    print('----------------------------')
    print('Problem name: ' + parser.problem_name)
    print('Objects: ' + str(parser.objects))
    print('State: ' + str([list(i) for i in parser.state]))
    print('Positive goals: ' + str([list(i) for i in parser.positive_goals]))
    print('Negative goals: ' + str([list(i) for i in parser.negative_goals]))

    start_time = time.time()
    planner = Planner(domain, problem)
    if len(sys.argv) > 3:
        if sys.argv[3] == "-opt":
            ec = planner.solve(style="opt")
        elif sys.argv[3] == "-bfs":
            ec = planner.solve(style="brute")
    else:
        ec = planner.solve()
    print('Time: ' + str(time.time() - start_time) + 's')
    exit(ec)
