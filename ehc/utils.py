import pddl_parser.PDDL as PDDL
import pddl_parser.planner as Pplanner

infty = 1e10

class PredNode:
    def __init__(self, info, parents=None, isaction=False):
        self._parents = parents # list of parents: default None (path root), must be list
        self._info = info
        self._sq = isaction # is this an action | default: False
    
    def __hash__(self):
        return hash(self._info)
    
    def __repr__(self):
        return "<Node: \nstate: %s \n is action?: %s>" % (repr(self._info), repr(self._sq))

    @property
    def parents(self):
        return self._parents
    
    @property
    def info(self):
        return self._info

    @property
    def sq(self):
        return self._sq

    def __eq__(self, other):
        return isinstance(other, PredNode) and isinstance(other._info, type(self._info)) and self._info == other._info

class Node:
    def __init__(self, state, action=None, cost=0, parent=None):
        self._parent = parent # parent node: default None (path root)
        self._state = state
        self._action = action # action to arrive at state; default None (path root)
        self._cost = cost # h(s)+g(s) for node; default 0 (path root)

    def __hash__(self):
        return hash(self._state)

    def __repr__(self):
        return "Node: \nstate: %s \n action: %s \n cost: %s" % (repr(self.state),
                                                                repr(self.action),
                                                                repr(self.cost))

    @property
    def parent(self):
        return self._parent

    @property
    def state(self):
        return self._state
    
    @property
    def cost(self):
        return self._cost
    
    @property
    def action(self):
        return self._action

    def __eq__(self, other):
        return isinstance(other, Node) and self._state == other._state
    
    def __lt__(self, other):
        return isinstance(other, Node) and self._cost < other._cost

    def __le__(self, other):
        return isinstance(other, Node) and self._cost <= other._cost

    def __gt__(self, other):
        return isinstance(other, Node) and self._cost > other._cost

    def __ge__(self, other):
        return isinstance(other, Node) and self._cost >= other._cost

class World:
    def __init__(self, domain, problem):
        self.domain = domain
        self.problem = problem
        self.prs = PDDL.PDDL_Parser() # Parser, shorthand: prs
        self.parse()

    def _parse_domain(self):
        self.prs.parse_domain(self.domain)
    
    def _parse_problem(self):
        self.prs.parse_problem(self.problem)
        self.start = Node(self.prs.state)
        self.goal = self.prs.positive_goals

    def parse(self):
        self._parse_domain()
        self._parse_problem()

    def ground_actions(self):
        self.g_acts = list()
        for action in self.prs.actions:
            for act in action.groundify(self.prs.objects, self.prs.types):
                self.g_acts.append(act)
