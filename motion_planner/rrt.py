import sys, os
from motion_planner.utils import *
import numpy as np

docker_prefix= "/home/shashank/Documents/JP/MIT/16.413/padm-project-f22"
jake_prefix="/home/jake/padm-project-f22"
sys.path.extend([os.path.abspath(docker_prefix)])
sys.path.extend([os.path.abspath("padm-project-2022f")])
sys.path.extend(os.path.abspath(os.path.join(docker_prefix+"/padm-project-2022f/", d)) for d in ['pddlstream', 'ss-pybullet', 'src'])
# sys.path.extend(os.path.abspath(os.path.join(os.getcwd(), d)) for d in ['pddlstream', 'ss-pybullet'])

from pybullet_tools.utils import set_pose, Pose, Point, Euler, multiply, get_pose, get_point, create_box, set_all_static, WorldSaver, create_plane, COLOR_FROM_NAME, stable_z_on_aabb, pairwise_collision, elapsed_time, get_aabb_extent, get_aabb, create_cylinder, set_point, get_function_name, wait_for_user, dump_world, set_random_seed, set_numpy_seed, get_random_seed, get_numpy_seed, set_camera, set_camera_pose, link_from_name, get_movable_joints, get_joint_name, set_renderer
from pybullet_tools.utils import CIRCULAR_LIMITS, get_custom_limits, set_joint_positions, interval_generator, get_link_pose, interpolate_poses, single_collision

from pybullet_tools.ikfast.franka_panda.ik import PANDA_INFO, FRANKA_URDF
from pybullet_tools.ikfast.ikfast import get_ik_joints, closest_inverse_kinematics

from src.world import World
from src.utils import JOINT_TEMPLATE, BLOCK_SIZES, BLOCK_COLORS, COUNTERS, \
    ALL_JOINTS, LEFT_CAMERA, CAMERA_MATRIX, CAMERA_POSES, CAMERAS, compute_surface_aabb, \
    BLOCK_TEMPLATE, name_from_type, GRASP_TYPES, SIDE_GRASP, joint_from_name, \
    STOVES, TOP_GRASP, randomize, LEFT_DOOR, point_from_pose, translate_linearly


class RRT:

    def __init__(self, world, goal):
        self.world = world
        self.goal = np.asarray(goal)

    def solve_path(self, init_config, max_samples=5000, sample_goal_interval=5, max_distance=1):
        root_config = ArmConfigVertex(init_config, None, [])
        config_tree = ArmConfigTree(root_config)
        for i in range(0, max_samples):
            sample_config = self.get_sample(self.world.robot, self.world.arm_joints)
            if i % sample_goal_interval == 0:
                sample_config = self.goal

            closest = config_tree.find_closest_node(sample_config)
            steered_config = closest.steer_toward(sample_config, max_distance)
            steered_vertex = ArmConfigVertex(steered_config, closest, [])

            if self.no_collisions(closest.config, steered_config):
                config_tree.add_vertex(steered_vertex)
                closest.add_child(steered_vertex)
                if (steered_config == self.goal).all():
                    path = self.extract_path(steered_vertex)
                    set_joint_positions(self.world.robot, self.world.arm_joints, init_config)
                    return path
        print("Failed to find path\nStarted: {}\nGoal: {}\nNum Samples:{}".format(init_config, self.goal, max_samples))
        #config_tree.print_tree()
        set_joint_positions(self.world.robot, self.world.arm_joints, init_config)
        return None

    def extract_path(self, steered_vertex):
        path = []
        current_node = steered_vertex
        while current_node is not None:
            path.insert(0, current_node.config)
            current_node = current_node.parent
        return path

    def get_sample(self, body, joints, custom_limits={}, **kwargs):
        lower_limits, upper_limits = get_custom_limits(body, joints, custom_limits,
                                                       circular_limits=CIRCULAR_LIMITS)


        rands = np.random.rand(len(lower_limits))
        for i in range(len(rands)):
            rands[i] = lower_limits[i] + (upper_limits[i] - lower_limits[i])*rands[i]
        return rands

    def interpolate_configs(self, start_config, end_config, num_steps=10):
        """
        loosely based on pybullet_tools/utils.py but rewritten for configuration space
        """

        out = []
        for i in range(num_steps):
            fraction = float(i)/num_steps
            config = (1-fraction)*start_config + fraction*end_config
            out.append(config)
        return out

    def no_collisions(self, start_config, end_config):
        """
        determines if there is a collision when travelling from start_config to end_config
        Requires fk along the interval between configs
        """
        for p in self.interpolate_configs(start_config, end_config):
            set_joint_positions(self.world.robot, self.world.arm_joints, p)
            if single_collision(self.world.robot):
                return False
        return True


