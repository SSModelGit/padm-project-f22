import numpy as np


class ArmConfigVertex:

    def __init__(self, config, parent, children):
        self.config = np.asarray(config)
        self.parent = parent
        self.children = children

    def dist2config(self, config):
        """
        Note that dist here is distance in the configuration space, and does not correspond to meters travelled
        """
        return np.linalg.norm(self.config-config)

    def add_child(self, child):
        self.children.append(child)

    def steer_toward(self, config, max_dist):
        """
        Note that dist here is distance in the configuration space, and does not correspond to meters travelled
        """
        dist = self.dist2config(config)
        if dist < max_dist:
            return config
        else:
            return (config-self.config)*(max_dist/dist) + self.config


class ArmConfigTree:

    def __init__(self, root):
        self.vertices = [root]
        self.root = root

    def find_closest_node(self, config):
        min_dist = None
        closest_node = None
        for v in self.vertices:
            current_dist = v.dist2config(config)
            if min_dist is None or min_dist > current_dist:
                min_dist = current_dist
                closest_node = v
        return closest_node

    def add_vertex(self, v):
        self.vertices.append(v)

    def print_tree(self):
        for v in self.vertices:
            print("Config: {}, with parent: {}".format(v.config, v.parent))
            for c in v.children:
                print("with child: {}".format(c.config))