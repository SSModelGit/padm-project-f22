import os
import sys
import argparse
import numpy as np

docker_prefix= "/appsrc"
jake_prefix="/home/jake/padm-project-f22"
sys.path.extend([os.path.abspath(docker_prefix)])
sys.path.extend([os.path.abspath("padm-project-2022f")])
sys.path.extend(os.path.abspath(os.path.join(docker_prefix+"/padm-project-2022f/", d)) for d in ['pddlstream', 'ss-pybullet', 'src'])

from motion_planner.rrt import RRT
from trajectory_optimization.solver import TrajectorySolver
from src.world import World
from src.utils import JOINT_TEMPLATE, BLOCK_SIZES, BLOCK_COLORS, COUNTERS, \
    ALL_JOINTS, LEFT_CAMERA, CAMERA_MATRIX, CAMERA_POSES, CAMERAS, compute_surface_aabb, \
    BLOCK_TEMPLATE, name_from_type, GRASP_TYPES, SIDE_GRASP, joint_from_name, \
    STOVES, TOP_GRASP, randomize, LEFT_DOOR, point_from_pose, translate_linearly
import src.utils as sut
from ehc import solve
import pybullet as pb
import pybullet_tools.ikfast as pbik
import pybullet_tools.utils as pbtu
from pose_gen.utils import Actor

UNIT_POSE2D = (0., 0., 0.)

class Operator:
    def __init__(self, domain, problem, use_gui=False, full_kitchen=False, go_rand=False):
        self.planner = solve.Planner(domain, problem)
        if not self.planner.solve():
            print("... plan found")
            self.world = World(use_gui=use_gui, full_kitchen=full_kitchen)
            self.sugar_box = self._add_ycb(self.world, 'sugar_box',
                                           idx=0, counter=1, pose2d=(-0.2, 0.65, np.pi / 4))
            self.spam_box = self._add_ycb(self.world, 'potted_meat_can',
                                          idx=1, counter=0, pose2d=(0.2, 1.1, np.pi / 4))
            # pbtu.wait_for_user()
            self.world._update_initial()
            self.tool_link = pbtu.link_from_name(self.world.robot, 'panda_hand')
            self.joints = pbtu.get_movable_joints(self.world.robot)
            print('Base Joints', [pbtu.get_joint_name(self.world.robot, joint) for joint in self.world.base_joints])
            print('Arm Joints', [pbtu.get_joint_name(self.world.robot, joint) for joint in self.world.arm_joints])
            self.world.set_initial_conf()
            self.defdomain()
            if go_rand:
                self.go_random()
        else:
            print("Activity problem ill-defined, cannot find a solution.")

    def defdomain(self):
        self.domain = dict()
#         self.domain["r1"] = self.world.robot
#         self.domain["burner"] = "front_right_stove"
#         self.domain["counter"] = "indigo_tmp"
#         self.domain["indigo_surf"] = "indigo_drawer_top"
#         self.domain["indigo"] = "indigo_drawer_top_joint"
# #         self.domain["l1"] = self._adj_pose(pbtu.get_link_pose(self.world.kitchen,56),[0.3,0.3])
# #         self.domain["l2"] = self._adj_pose(pbtu.get_link_pose(self.world.kitchen,56),[0.2,0])

        self.domain["l1"] = [[-1.265, 0.64, -1.2], [0.0, 0.0, 0.0, 1.0]]
        self.domain["l2"] = [[-1.15, 1.1, -1.2], [0,0,0,1.0]]
        self.domain["spam"] = {"spam": "potted_meat_can1",
                               "counter": [0.05, 0.7, 0, -2.2, 0, 4.2, 0.74],
                               "indigo": [0, 0.45, 0, -2.3, 0, 3, 0.74]}
        self.domain["sugar"] = {"sugar": 'sugar_box0',
                                "burner": [0, 1.7, 0, 0, 0, 3.1, 0.74],
                                "counter": [0, 1, 0, -1.9, 0, 4.5, 0.74]}
        self.domain["indigo"] = {"indigo": pbtu.joint_from_name(self.world.kitchen, "indigo_drawer_top_joint"),
                                "open": [0, 1.3, 0, -1.95, 0, 4.7, -0.7], # "open": [0, 1.7, 0, -0.7, 0, 3.9, -0.7],
                                 "close": [0, 1.3, 0, -1.95, 0, 4.7, -0.7]}
        self.delay = 0.1


    def _debug(self):
        loc = [[-1.15, 1.1, -1.2], [0,0,0,1.0]]
        dbg_0 = [0, 1.3, 0, -1.95, 0, 4.7, -0.7]
        dbg_1 = "sugar"
        dbg_2 = "counter"
        while (1):
            self.move_to_start(loc)
            pbtu.set_joint_positions(self.world.robot, self.world.arm_joints, self.domain[dbg_1][dbg_2])
            # pbtu.set_joint_positions(self.world.robot, self.world.arm_joints, dbg_0)

    def translate(self, optQ=False):
        actor = Actor(self.delay, opt=optQ)
        self.move_to_start(self.domain["l1"])
        # self._debug()
        grasp = None
        for act in self.planner.a_list:
            print(act.name, act.parameters)
            if act.name == "move":
                actor._move(self.world, self.domain[act.parameters[2]], grasp)
                pass
            elif act.name == "pick":
                grasp = actor._pick(self.world, 
                                      self.domain[act.parameters[3]][act.parameters[3]],
                                      self.domain[act.parameters[3]][act.parameters[4]])
            elif act.name == "take":
                grasp = actor._take(self.world,
                                      self.domain[act.parameters[3]][act.parameters[3]],
                                      self.domain[act.parameters[3]][act.parameters[4]])
            elif act.name == "put":
                actor._put(self.world, grasp, self.domain[act.parameters[3]][act.parameters[4]])
            elif act.name == "open":
                actor._open(self.world,
                            self.domain[act.parameters[3]][act.parameters[3]],
                            self.domain[act.parameters[3]][act.name])
            elif act.name == "stow":
                actor._stow(self.world, grasp, self.domain[act.parameters[3]][act.parameters[4]])
                actor._close(self.world,
                             self.domain[act.parameters[4]][act.parameters[4]],
                             self.domain[act.parameters[4]]["close"])
                grasp = None
                actor._move(self.world, self.domain[act.parameters[1]], grasp)
        self.move_to_end()

    def with_optimal(self):
        self.translate(optQ=False)
        self.world.initial_saver.restore()
        self.spam_box = self._add_ycb(self.world, 'potted_meat_can',
                                      idx=1, counter=0, pose2d=(0.2, 1.1, np.pi / 4))
        self.translate(optQ=True)

    def move_to_start(self, start_pose):
        actor = Actor(self.delay)
        actor._move(self.world, start_pose)

    def move_to_end(self):
        actor = Actor(self.delay)
        actor._move(self.world, [[1,0.3,-1.2],[0.0,0.0,0.0,1.0]])

    def _adj_pose(self, pose, dims):
        post = list(map(list,pose))
        post[0][0] += dims[0]
        post[0][1] += dims[1]
        return tuple(map(tuple, post))

    def _add_ycb(self, world, ycb_type, idx=0, counter=0, **kwargs):
        name = name_from_type(ycb_type, idx)
        world.add_body(name, color=np.ones(4))
        self._pose2d_on_surface(world, name, COUNTERS[counter], **kwargs)
        return name

    def _pose2d_on_surface(self, world, entity_name, surface_name, pose2d=UNIT_POSE2D):
        x, y, yaw = pose2d
        body = world.get_body(entity_name)
        surface_aabb = compute_surface_aabb(world, surface_name)
        z = pbtu.stable_z_on_aabb(body, surface_aabb)
        pose = pbtu.Pose(pbtu.Point(x, y, z), pbtu.Euler(yaw=yaw))
        pbtu.set_pose(body, pose)
        return pose

    def _get_sample_fn(self, body, joints, custom_limits={}, **kwargs):
        lower_limits, upper_limits = pbtu.get_custom_limits(body, joints, custom_limits,
                                                            circular_limits=pbtu.CIRCULAR_LIMITS)
        generator = pbtu.interval_generator(lower_limits, upper_limits, **kwargs)
        def fn():
            return tuple(next(generator))
        return fn

    def _start_random(self):
        self.sample_fn = self._get_sample_fn(self.world.robot, self.world.arm_joints)
        print("Going to use IK to go from a sample start state to a goal state\n")
        self.conf = self.sample_fn()
        pbtu.set_joint_positions(self.world.robot, self.world.arm_joints, self.conf)
        ik_joints = pbik.ikfast.get_ik_joints(self.world.robot, 
                                              pbik.franka_panda.ik.PANDA_INFO, self.tool_link)
        print(self.conf)
        self.start_pose = pbtu.get_link_pose(self.world.robot, self.tool_link)
        print(self.start_pose)

    def _rrt_random(self):
        end_pose = pbtu.multiply(self.start_pose, pbtu.Pose(pbtu.Point(z=1)))
        end_conf = self.sample_fn()
        # motion planner call (RRT)
        motion_planner = RRT(self.world, end_conf)
        self.path = motion_planner.solve_path(self.conf)
        # Act out RRT path
        pbtu.wait_for_user()
        pbtu.wait_for_duration(10)
        bsaver = pbtu.BodySaver(self.world.robot)
        for cnf in self.path:
            pbtu.set_joint_positions(self.world.robot, self.world.arm_joints, cnf)
            pbtu.wait_for_duration(0.2)
        # Restore
        pbtu.wait_for_duration(5)
        bsaver.restore()
        # motion planner call (optimizer)
        optimized_planner = TrajectorySolver(self.conf, None, 2, len(self.path), end_conf)
        path = optimized_planner.solve(self.world, np.array(self.path))
        # Act out optimized path
        for cnf in path.T:
            pbtu.set_joint_positions(self.world.robot, self.world.arm_joints, cnf)
            pbtu.wait_for_duration(0.2)
        pbtu.wait_for_user()

    def go_random(self):
        self.move_to_start(self.domain['l1'])
        self._start_random()
        self._rrt_random()

if __name__ == '__main__':
    domain = "domain.pddl"
    problem = "base_prob.pddl"
    pw = Operator(domain, problem, use_gui=True, go_rand=False)
    optimalQ = True
    if not optimalQ:
        pw.translate()
    else:
        pw.with_optimal()
    print("Finishing.")