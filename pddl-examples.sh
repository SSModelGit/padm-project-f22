#!/bin/bash

python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/blocksworld/blocksworld.pddl padm-project-2022f/pddl-parser/examples/blocksworld/pb1.pddl
python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/blocksworld/blocksworld.pddl padm-project-2022f/pddl-parser/examples/blocksworld/pb2.pddl
python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/blocksworld/blocksworld.pddl padm-project-2022f/pddl-parser/examples/blocksworld/pb3.pddl
python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/blocksworld/blocksworld.pddl padm-project-2022f/pddl-parser/examples/blocksworld/pb4.pddl
python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/blocksworld/blocksworld.pddl padm-project-2022f/pddl-parser/examples/blocksworld/pb5.pddl
python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/blocksworld/blocksworld.pddl padm-project-2022f/pddl-parser/examples/blocksworld/pb6.pddl

python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/dinner/dinner.pddl padm-project-2022f/pddl-parser/examples/dinner/pb1.pddl

python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/tsp/tsp.pddl padm-project-2022f/pddl-parser/examples/tsp/pb1.pddl

python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/dwr/dwr.pddl padm-project-2022f/pddl-parser/examples/dwr/pb1.pddl
python -B -m ehc.solve padm-project-2022f/pddl-parser/examples/dwr/dwr.pddl padm-project-2022f/pddl-parser/examples/dwr/pb2.pddl
