import os
import sys
import argparse
import numpy as np

docker_prefix= "/appsrc"
jake_prefix="/home/jake/padm-project-f22"
sys.path.extend([os.path.abspath(docker_prefix)])
sys.path.extend([os.path.abspath("padm-project-2022f")])
sys.path.extend(os.path.abspath(os.path.join(docker_prefix+"/padm-project-2022f/", d)) for d in ['pddlstream', 'ss-pybullet', 'src'])

from motion_planner.rrt import RRT
from trajectory_optimization.solver import TrajectorySolver
from src.world import World
from src.utils import JOINT_TEMPLATE, BLOCK_SIZES, BLOCK_COLORS, COUNTERS, \
    ALL_JOINTS, LEFT_CAMERA, CAMERA_MATRIX, CAMERA_POSES, CAMERAS, compute_surface_aabb, \
    BLOCK_TEMPLATE, name_from_type, GRASP_TYPES, SIDE_GRASP, joint_from_name, \
    STOVES, TOP_GRASP, randomize, LEFT_DOOR, point_from_pose, translate_linearly
import src.utils as sut
from ehc import solve
import pybullet as pb
import pybullet_tools.ikfast as pbik
import pybullet_tools.utils as pbtu

class Actor:
    def __init__(self, delay=0.1, subdiv=100, opt=False):
        self.delay = delay
        self.n = subdiv
        self.opt = opt
    
#     def _move(self, world, locs, loce, grasp=None):
#         """
#         locs: start location (pose)
#         loce:   end location (pose)
#         """
#         # sconf = self.get_curr_conf(world)
#         # tconf = self.get_goal_conf(world, loce)
#         sconf = locs
#         tconf = loce
#         path = self.create_path(world, sconf, tconf)
#         for cnf in path:
#             pbtu.set_joint_positions(world.robot, world.arm_joints, cnf)
#             if grasp:
#                 grasp.assign()

    def _move(self, world, tpose, grasp=None):
        """
        tpose: target base pose
        """
        # setup
        tconf = tpose[0]
        cconf = pbtu.get_point(world.robot)
        cx = cconf[0]
        cy = cconf[1]
        tz = tconf[2]
        # subdivide
        xd = (tconf[0] - cconf[0]) / self.n
        yd = (tconf[1] - cconf[1]) / self.n
        # iterate to end pose
        for i in range(self.n):
            cx += xd
            cy += yd
            pbtu.set_pose(world.robot, [[cx, cy, tz], tpose[1]])
            if grasp:
                self._update_grasp(world, grasp)
            pbtu.wait_for_duration(self.delay/10)
        pbtu.set_pose(world.robot, tpose)

    def _pick(self, world, body_name, tconf):
        # tpose = pbtu.get_pose(world.body_from_name[body_name])
        # tconf = self.get_goal_conf(world, tpose)

        # open gripper to start
        world.open_gripper()
        # shift backwards to give space for planning
        shift = -0.4 # HERE ONLY positive moves forwards (even though pos pose values go backwards)
        cpose = pbtu.get_pose(world.robot)
        for i in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]-(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            pbtu.wait_for_duration(self.delay/10)

        # plan for opening posture
        cconf = pbtu.get_joint_positions(world.robot, world.arm_joints)
        path = self.create_path(world, cconf, tconf)
        self.check_path(world, path, tconf)
        self.follow_path(world, path, delay=self.delay)

        # shift back into place
        for j in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]+(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            pbtu.wait_for_duration(self.delay/10)

        world.close_gripper()
        return [world.tool_link, world.body_from_name[body_name]]

    def _take(self, world, body_name, tconf):
        return self._pick(world, body_name, tconf)

    def _put(self, world, grasp, tconf):
        # body = grasp.child
        # surface_aabb = compute_surface_aabb(world, surf)
        # z = pbtu.stable_z_on_aabb(body, surface_aabb)
        # tpose = pbtu.Pose(pbtu.Point(x, y, z), pbtu.Euler(yaw=yaw))

        # world.open_gripper()
        world.close_gripper()
        # shift backwards to give space for planning
        shift = -0.4 # HERE ONLY positive moves forwards (even though pos pose values go backwards)
        cpose = pbtu.get_pose(world.robot)
        for i in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]-(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            gpose = pbtu.get_pose(grasp[1])
            pbtu.set_pose(grasp[1], [[gpose[0][0]-(shift/self.n),gpose[0][1],gpose[0][2]],gpose[1]])
            pbtu.wait_for_duration(self.delay/10)

        # plan for opening posture (temporarily kick held objects upwards)
        gpose = pbtu.get_pose(grasp[1])
        pbtu.set_pose(grasp[1], [[gpose[0][0],gpose[0][1],gpose[0][2]+100],gpose[1]])
        cconf = pbtu.get_joint_positions(world.robot, world.arm_joints)
        path = self.create_path(world, cconf, tconf)
        self.check_path(world, path, tconf)
        gpose = pbtu.get_pose(grasp[1])
        pbtu.set_pose(grasp[1], [[gpose[0][0],gpose[0][1],gpose[0][2]-100],gpose[1]])

        # shift back into place
        for j in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]+(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            gpose = pbtu.get_pose(grasp[1])
            pbtu.set_pose(grasp[1], [[gpose[0][0]+(shift/self.n),gpose[0][1],gpose[0][2]],gpose[1]])
            pbtu.wait_for_duration(self.delay/10)

        # world.close_gripper()
        self.follow_path(world, path, grasp = grasp, delay=self.delay)
        world.open_gripper()
    
    def _stow(self, world, grasp, tconf):
        self._put(world, grasp, tconf)
        pbtu.remove_body(grasp[1])
        del world.body_from_name["potted_meat_can1"]
    
    def _open(self, world, joint2, conf):
        """
        body2: body of joint to be opened
        j2:    joint to be opened
            Example: _open(world, world.kitchen, "indigo_drawer_top_joint")
        """
#         joint2 = pbtu.joint_from_name(body2,j2)
#         loc2 = pbtu.get_link_pose(body2,joint2)
#         loc2e = list(map(list,loc2))
#         loc2e[0][0] = world.open_conf(joint2)
#         loc2e = tuple(map(tuple, loc2e))
#         tconf = self.get_goal_conf(world, loc2e)

        # open gripper to start
        world.open_gripper()
        # shift backwards to give space for planning
        shift = -0.2 # HERE ONLY positive moves forwards (even though pos pose values go backwards)
        cpose = pbtu.get_pose(world.robot)
        for i in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]-(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            pbtu.wait_for_duration(self.delay/10)
        
        # plan for opening posture
        cconf = pbtu.get_joint_positions(world.robot, world.arm_joints)
        path = self.create_path(world, cconf, conf)
        self.check_path(world, path, conf)
        self.follow_path(world, path, delay=self.delay)

        # shift back into place
        for j in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]+(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            pbtu.wait_for_duration(self.delay/10)

        # close and pull backwards
        world.close_gripper()
        shift = -0.15 # HERE ONLY positive moves forwards (even though pos pose values go backwards)
        cpose = pbtu.get_pose(world.robot)
        spose = cpose
        for i in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]-(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            jposi = pbtu.get_joint_position(world.kitchen, joint2)
            pbtu.set_joint_position(world.kitchen, joint2, jposi-shift/self.n)
            pbtu.wait_for_duration(self.delay/10)
        # release drawer
        world.open_gripper()
        # go to vert position, prevent collision with drawer
        cconf = pbtu.get_joint_positions(world.robot, world.arm_joints)
        path = self.create_path(world, cconf, [0,0,0,0,0,0,0])
        self.follow_path(world, path, delay=self.delay)
        # move back into position
        for j in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]+(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            pbtu.wait_for_duration(self.delay/10)
        pbtu.set_pose(world.robot, spose)
            
    def _close(self, world, joint2, conf):
        """
        Ex: _open(world, world.kitchen, "indigo_drawer_top_joint")
        """
#         joint2 = pbtu.joint_from_name(body2,j2)
#         loc2 = pbtu.get_link_pose(body2,j2)
#         loc2e = list(map(list,loc2))
#         loc2e[0][0] = world.closed_conf(joint2)
#         loc2e = tuple(map(tuple, loc2e))
#         sconf = self.get_curr_conf(world)
#         tconf = self.get_goal_conf(world, loc2e)
#         path = self.create_path(world, sconf, tconf)
#         world.close_gripper()
#         for cnf in path:
#             pbtu.set_joint_positions(world.robot, world.arm_joints, cnf)
#             pbtu.set_joint_position(body2, joint2, cnf[-1])
#         world.open_gripper()
#         tconf = self.get_goal_conf(world, loc2e)
        world.open_gripper()
        # shift backwards to give space for planning
        shift = -0.4 # HERE ONLY positive moves forwards (even though pos pose values go backwards)
        cpose = pbtu.get_pose(world.robot)
        for i in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]-(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            pbtu.wait_for_duration(self.delay/10)
        
        # plan for opening posture
        cconf = pbtu.get_joint_positions(world.robot, world.arm_joints)
        path = self.create_path(world, cconf, conf)
        self.check_path(world, path, conf)
        self.follow_path(world, path, delay=self.delay)

        shift = -0.25 # arrive at drawer opening
        # shift back into place
        for j in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]+(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            pbtu.wait_for_duration(self.delay/10)

        # close and push forwards
        world.close_gripper()
        shift = -0.15 # HERE ONLY positive moves forwards (even though pos pose values go backwards)
        # move back into position
        for j in range(self.n):
            pbtu.set_pose(world.robot, [[cpose[0][0]+(shift/self.n),cpose[0][1],cpose[0][2]],cpose[1]])
            cpose = pbtu.get_pose(world.robot)
            jposi = pbtu.get_joint_position(world.kitchen, joint2)
            pbtu.set_joint_position(world.kitchen, joint2, jposi+shift/self.n)
            pbtu.wait_for_duration(self.delay/10)
        world.open_gripper()

        # go to vert position, prevent collision with drawer
        cconf = pbtu.get_joint_positions(world.robot, world.arm_joints)
        path = self.create_path(world, cconf, [0,0,0,0,0,0,0])
        self.follow_path(world, path, delay=self.delay)

    def create_path(self, world, sconf, tconf):
        """
        tconf: target configuration for end effector
        """
        pbtu.set_renderer(enable=False)
        motion_planner = RRT(world, tconf)
        path = motion_planner.solve_path(sconf)
        if self.opt:
            optimized_planner = TrajectorySolver(sconf, None, 2, len(path), tconf)
            opt_path = optimized_planner.solve(world, np.array(path))
            pbtu.set_renderer(enable=True)
            if opt_path is None:
                print("optimizer failed.")
                return path
            return opt_path.T
        pbtu.set_renderer(enable=True)
        return path

    def check_path(self, world, path, conf):
        if path is None:
            pbtu.set_joint_positions(world.robot, world.arm_joints, conf)
            raise Exception('RRT failed to find a path, returned None. Manually set to final position.')

    def follow_path(self, world, path, grasp=None, delay=0.1):
        for cnf in path:
            pbtu.set_joint_positions(world.robot, world.arm_joints, cnf)
            if grasp:
                self._update_grasp(world, grasp)
            pbtu.wait_for_duration(delay)

    def get_goal_conf(self, world, tpose):
        """
        tpose: target pose for end effector
        """
        # tl = pbtu.link_from_name(world.robot, 'panda_hand')
        tl = world.tool_link
        tp_list = tuple(pbtu.interpolate_poses(pbtu.get_link_pose(world.robot, tl), tpose, pos_step_size=0.01))
        print("target", tpose[0])
        # print("interp", [x[0] for x in tp_list])
        pbtu.draw_pose(pose=tpose, parent=world.robot, parent_link=tl, length=3)
        pbtu.wait_for_user()
        sol_confs = pbtu.plan_cartesian_motion(world.robot, world.arm_joints[0], tl, tp_list, max_iterations=200)
        print(sol_confs, len(sol_confs))
        pbtu.wait_for_user()
        return [sol_confs[-1][i-3] for i in world.arm_joints]

    def _update_grasp(self, world, grasp):
        grasper = grasp[0]
        grasped = grasp[1]
        grasper_point = pbtu.get_link_pose(world.robot, grasper)[0]
        grasped_quat = pbtu.get_pose(grasped)[1]
        pbtu.set_pose(grasped, [grasper_point, grasped_quat])