import numpy as np

from pydrake.solvers import MathematicalProgram, Solve

from pybullet_tools.utils import set_pose, Pose, Point, Euler, multiply, get_pose, get_point, create_box, set_all_static, WorldSaver, create_plane, COLOR_FROM_NAME, stable_z_on_aabb, pairwise_collision, elapsed_time, get_aabb_extent, get_aabb, create_cylinder, set_point, get_function_name, wait_for_user, dump_world, set_random_seed, set_numpy_seed, get_random_seed, get_numpy_seed, set_camera, set_camera_pose, link_from_name, get_movable_joints, get_joint_name
from pybullet_tools.utils import CIRCULAR_LIMITS, get_custom_limits, set_joint_positions, interval_generator, get_link_pose, interpolate_poses, single_collision


def config_dist(configs):
    return np.abs(configs[:7] - configs[7:])
    #return np.array([np.linalg.norm(configs[:7] - configs[7:])])
class TrajectorySolver:

    def __init__(self, start, obstalces, max_dist, time_steps, end):
        self.start = start
        self.obs = obstalces
        self.max_dist = max_dist
        self.time_steps = time_steps
        self.end = end

    def solve(self, world, init_guess):

        prog = MathematicalProgram()
        angles = prog.NewContinuousVariables(7, self.time_steps)

        def obs_constraint(config):
            set_joint_positions(world.robot, world.arm_joints, config)
            if single_collision(world.robot):
                return float(0)
            else:
                return float(1)
        for t in range(self.time_steps):
            if t < self.time_steps-1:
                dist_constraint = prog.AddConstraint(
                    config_dist,
                    lb = np.array([0.]*7),
                    ub = np.array([float(self.max_dist)]*7),
                    vars = angles[:,t:t+2].flatten(),
                    description="dist"
                )
            #collision_constraint = prog.AddConstraint(
            #    obs_constraint,
            #    lb = np.array([1]),
            #    ub = np.array([1]),
            #    vars = angles[:,t],
            #    description="collision"
            #)
        #print(dist_constraint)
        #print(collision_constraint)
        def start_constraint(config):
            return self.start - config

        start_constraint = prog.AddConstraint(
            start_constraint,
            lb=np.ones(7) * -0.,  # np.array([0, 0, 0, 0, 0, 0, 0]),
            ub=np.ones(7) * 0.,  # np.array([0, 0, 0, 0, 0, 0, 0]),
            vars = angles[:,0]
        )

        def end_constraint(config):
            return self.end - config

        end_constraint = prog.AddConstraint(
            end_constraint,
            lb=np.ones(7)*-0.,#np.array([0, 0, 0, 0, 0, 0, 0]),
            ub=np.ones(7)*0.,#np.array([0, 0, 0, 0, 0, 0, 0]),
            vars=angles[:, -1]
        )

        def dist_cost(configs):
            cost = 0
            configs = configs.reshape(7,-1)
            for t in range(1,self.time_steps):
                cost += config_dist(configs[:,t-1:t+1].flatten())[0]
            return cost

        prog.AddCost(dist_cost, vars=angles.flatten())

        print("guess shape: {}".format(init_guess.shape))
        print("angles shape: {}".format(angles.shape))
        print("len path: {}".format(self.time_steps))
        print(f"initial start: {init_guess[0,:]}, start: {self.start}")
        print(f"initial end: {init_guess[-1,:]}, end: {self.end}")
        prog.SetInitialGuess(angles,init_guess.T)

        result = Solve(prog, init_guess.flatten())
        print(f"success: {result.is_success()}")
        if result.is_success():
            path = np.array(result.GetSolution(angles))
            print(f"solution:\n{path.reshape(7,-1)}")
            print(f"init guess:\n{init_guess.T}")
            return path
        return None


